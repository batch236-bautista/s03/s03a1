package com.zuitt.example;
import java.util.Scanner;

public class s3A1 {
    private static Scanner sc;

    public static void main(String[] args) {

        int i, Number;
        long Fctl = 1;
        sc = new Scanner(System.in);

        System.out.println(" Input an integer whose factorial will be computed: ");
        Number = sc.nextInt();

        for (i = 1; i <= Number; i++)  {
            Fctl = Fctl * i;
        }
        System.out.format("\nThe factorial of %d is %d\n", Number, Fctl);
    }
}
